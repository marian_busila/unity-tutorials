## Proof of concepts created in Unity3D.

* FlockingFish

![FlockingFish](images/FlockingFish.gif)

* MoveOnRail

![MoveOnRail](images/MoveOnRail.gif)

* GetInAVehicle

![GetInAVehicle](images/GetInAVehicle.gif)

* CubeGrid - smashing a grid of cubes

![CubeGrid](images/CubeGrid.gif)

* CubeBlendShapes - a proof of concept for blend shapes

![CubeBlendShapes](images/CubeBlendShapes.gif)